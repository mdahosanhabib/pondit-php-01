<?php

class GradCalculator
{
    public $bangla;
    public $english;
    public $math;

    public function setMarks($data)
    {
        if (array_key_exists('bangla', $data)){
            $this->bangla = $data['bangla'];
        }
        if (array_key_exists('english', $data)){
            $this->english = $data['english'];
        }
        if (array_key_exists('math', $data)){
            $this->math = $data['math'];
        }
    }

    public function averageMark(){
        return  ($this->bangla + $this->english + $this->math) / 3;
    }

    public function gpa()
    {
        $averageMark = $this->averageMark();

       if($averageMark < 33){
           echo "failed";
       }else{
           if($averageMark > 33 && $averageMark < 40){
               echo "C Grad";
           }elseif ($averageMark > 39 && $averageMark < 50){
               echo "B Grad";
           }
       }
    }
}