<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 2/28/2018
 * Time: 2:27 PM
 */

namespace Pondit\Book;

use PDO;

class Book
{
    public $title;
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $conn;

    public function __construct()
    {
        session_start();
        try{
            $this->conn = new PDO("mysql:host=$this->servername;dbname=crud", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `books`";
        $stmt = $this->conn->query($sql);
        $data = $stmt->fetchAll();
        return $data;
    }

    public function setData($data)
    {
        if(array_key_exists('title', $data) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
    }

    public function store()
    {
        $sql =  "INSERT INTO books(title) values('$this->title')";
        $this->conn->exec($sql);
        $_SESSION['message'] = "New record created successfully";
        header('location: index.php');
    }

}