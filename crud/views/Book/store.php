<?php
include_once('../../vendor/autoload.php');

use Pondit\Book\Book;

$book = new Book();

if(array_key_exists('title', $_POST) && !empty($_POST['title'])) {
    $book->setData($_POST);
    $book->store();
}else{
    $_SESSION['error'] = 'Title should not be empty';
    header('location: create.php');
}



