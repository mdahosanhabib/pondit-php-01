<?php
include_once('../../vendor/autoload.php');

use Pondit\Book\Book;

$book = new Book();

$books = $book->index();
?>

<h1>Book List</h1>
<a href="create.php">Add New</a><hr>
<?php
if (isset($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>

<table border="1" cellpadding="5">
    <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sl = 0;
        foreach ($books as $book){?>
            <tr>
                <td><?= ++$sl ?></td>
                <td><?= $book['title'] ?></td>
            </tr>
        <?php
        }?>
    </tbody>
</table>