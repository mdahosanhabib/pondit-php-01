<?php
session_start();

echo $_SESSION['name'];

echo "<h1>Understanding Control Structure (if/else)</h1>";

$x = 10;

if($x != 10){
    echo 'True';
} elseif($x > 19){
    echo 'Its true';
} elseif($x > 24){
    echo 'Its also true';
} else{
    echo 'False';
}
