<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Understanding Super Global Variables</title>
</head>
<body>
    <h1>Understanding Super Global Variables</h1>
    <form action="" method="POST">
        <input type="number"  name="number1" placeholder="Enter Number 1">
        <input type="number"  name="number2" placeholder="Enter Number 2">
        <button type="submit" name="add">+</button>
        <button type="submit" name="sub">-</button>
        <button type="submit">C</button>
    </form>
</body>
</html>

<?php
session_start();
echo $_SESSION['name'];

if($_SERVER["REQUEST_METHOD"] == "POST"){
    echo "<pre>";
    if(isset($_POST['add'])){
        echo $_POST['number1'] + $_POST['number2'];
    }elseif(isset($_POST['sub'])){
        echo $_POST['number1'] - $_POST['number2'];
    }else{
        echo 'No calculation';
    }

//    ."<br>";
//
}

