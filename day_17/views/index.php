<?php
require_once('../vendor/autoload.php');

use Pondit\Bangla\Video as BanglaVideo;
use Pondit\English\Video as EnglishVideo;

echo "<h1>Understanding Namespace and Composer</h1>";


$bangla = new BanglaVideo();
echo "<br/>";
$english = new EnglishVideo();