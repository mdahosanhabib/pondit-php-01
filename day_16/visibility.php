<?php
echo "<h1>Understanding Inheritance & Visibility</h1>";

class Calculate
{
    public $gpa = 5;
    private $mark = null;
    protected $roll = null;
//    private $roll = '';

    public function getGpa()
    {
        return  $this->gpa;
    }

    public function setMark($mark)
    {
         $this->mark = $mark;
    }

    public function getMark()
    {
         return $this->mark;
    }

    public function setRoll($roll)
    {
        $this->roll = $roll;
    }

    public function getRoll()
    {
        return $this->roll;
    }
}

class Grad extends Calculate
{

    public $result = '';

    public function showResult()
    {
       return $this->roll;
    }

}


$object = new Calculate();

echo $object->getGpa();
echo "<br/>";
$object->setMark(60);
echo $object->getMark();
echo "<br/>";
echo $object->setRoll(123);
echo "<br/>";
echo $object->getRoll(123);
echo "<br/>";
echo $object->gpa;

echo "<hr/>Grade <br/>";

$grad = new Grad();

echo $grad->gpa;
echo "<br/>";
echo $grad->setRoll(563);
echo "<br/>";
echo $grad->showResult();


